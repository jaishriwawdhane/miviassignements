package com.axxara.axxaraapps.receivers;

/**
 * Created by jaishri on 3/10/18.
 */

public interface ScanResultReceiver {
    /**
     * function to receive scanresult
     * @param codeFormat format of the barcode scanned
     * @param codeContent data of the barcode scanned
     */
    public void scanResultData(String codeFormat, String codeContent);

}
