
package com.axxara.axxaraapps.qrlinkdata;

import java.util.List;

public class QRData {

    private List<Double> latLng = null;
    private Data data;

    public QRData(List<Double> latLng, Data data) {
        this.latLng = latLng;
        this.data = data;
    }

    public List<Double> getLatLng() {
        return latLng;
    }

    public void setLatLng(List<Double> latLng) {
        this.latLng = latLng;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
