
package com.axxara.axxaraapps.qrlinkdata;

import java.util.List;

public class Data {

    private List<Double> latLng = null;
    private String port1Status;
    private String closeTime;
    private String stationADAStatus;
    private String stationId;
    private String zipCode;
    private String stationStatus;
    private int portQuantity;
    private int vendingprice1;
    private String freeTrail;
    private int vendingPriceUnit1;
    private String openTime;
    private String stationAddress;
    private String stationName;
    private String notes;
    private String stationType;
    private String conn1portLevel;
    private String parkingPricePH;

    public List<Double> getLatLng() {
        return latLng;
    }

    public void setLatLng(List<Double> latLng) {
        this.latLng = latLng;
    }

    public String getPort1Status() {
        return port1Status;
    }

    public void setPort1Status(String port1Status) {
        this.port1Status = port1Status;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getStationADAStatus() {
        return stationADAStatus;
    }

    public void setStationADAStatus(String stationADAStatus) {
        this.stationADAStatus = stationADAStatus;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getStationStatus() {
        return stationStatus;
    }

    public void setStationStatus(String stationStatus) {
        this.stationStatus = stationStatus;
    }

    public int getPortQuantity() {
        return portQuantity;
    }

    public void setPortQuantity(int portQuantity) {
        this.portQuantity = portQuantity;
    }

    public int getVendingprice1() {
        return vendingprice1;
    }

    public void setVendingprice1(int vendingprice1) {
        this.vendingprice1 = vendingprice1;
    }

    public String getFreeTrail() {
        return freeTrail;
    }

    public void setFreeTrail(String freeTrail) {
        this.freeTrail = freeTrail;
    }

    public int getVendingPriceUnit1() {
        return vendingPriceUnit1;
    }

    public void setVendingPriceUnit1(int vendingPriceUnit1) {
        this.vendingPriceUnit1 = vendingPriceUnit1;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getStationAddress() {
        return stationAddress;
    }

    public void setStationAddress(String stationAddress) {
        this.stationAddress = stationAddress;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getStationType() {
        return stationType;
    }

    public void setStationType(String stationType) {
        this.stationType = stationType;
    }

    public String getConn1portLevel() {
        return conn1portLevel;
    }

    public void setConn1portLevel(String conn1portLevel) {
        this.conn1portLevel = conn1portLevel;
    }

    public String getParkingPricePH() {
        return parkingPricePH;
    }

    public void setParkingPricePH(String parkingPricePH) {
        this.parkingPricePH = parkingPricePH;
    }

}
