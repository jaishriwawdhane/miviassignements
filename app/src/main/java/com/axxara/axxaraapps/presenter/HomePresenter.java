package com.axxara.axxaraapps.presenter;

import android.content.Intent;
import android.view.View;

import com.axxara.axxaraapps.R;
import com.axxara.axxaraapps.contract.HomeContract;
import com.axxara.axxaraapps.model.HomeModels;
import com.axxara.axxaraapps.view.ChartActivity;
import com.axxara.axxaraapps.view.QRActivity;

/**
 * Created by jaishri on 29/9/18.
 */

public class HomePresenter implements HomeContract.HomePresenter {
    private HomeContract.HomeView homeView;
    private HomeContract.HomeModel homeModel;

    public HomePresenter(HomeContract.HomeView homeView) {
        this.homeView = homeView;
        initPresenter();
    }

    private void initPresenter() {
        homeModel = new HomeModels();
        homeView.initView();
    }


    @Override
    public void onButtonClicked(View view) {
        if (view.getId() == R.id.btnScanner)
            view.getContext().startActivity(new Intent(view.getContext().getApplicationContext(), QRActivity.class));
        else
            view.getContext().startActivity(new Intent(view.getContext().getApplicationContext(), ChartActivity.class));

    }
}
