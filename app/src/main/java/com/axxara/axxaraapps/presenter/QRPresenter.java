package com.axxara.axxaraapps.presenter;

import android.app.ProgressDialog;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.axxara.axxaraapps.contract.QRContract;
import com.axxara.axxaraapps.model.QRModel;
import com.axxara.axxaraapps.qrlinkdata.Data;
import com.axxara.axxaraapps.qrlinkdata.QRData;
import com.axxara.axxaraapps.view.QRActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by jaishri on 29/9/18.
 */

public class QRPresenter implements QRContract.MVPPresenter {

    private QRContract.MVPView mvpView;
    private QRContract.MVPModel mvpModel;
    private QRData data;

    public QRPresenter(QRContract.MVPView mvpView, QRData data) {
        this.mvpView = mvpView;
        this.data = data;
        initPresenter();
    }

    private void initPresenter() {
        mvpModel = new QRModel(data);
        mvpView.initView();
    }

    @Override
    public void voidOnScannedCode() {

        String lat1 = "", lng1 = "";
        String lat2 = "", lng2 = "";
        /*for (double lat : mvpModel.getData().getData().getLatLng()) {
            lat1 = String.valueOf(lat);

        }*/

        for (int i = 0; i < mvpModel.getData().getData().getLatLng().size(); i++) {
            lat1 = String.valueOf(mvpModel.getData().getLatLng().get(0));
            lng1 = String.valueOf(mvpModel.getData().getLatLng().get(1));
        }

        for (int i = 0; i < mvpModel.getData().getData().getLatLng().size(); i++) {
            lat2 = String.valueOf(mvpModel.getData().getData().getLatLng().get(0));
            lng2 = String.valueOf(mvpModel.getData().getData().getLatLng().get(1));
        }

        mvpView.setData(
                mvpModel.getData().getData().getPort1Status() +
                        "\n\nLatLng1: " + lat1 + ", " + lng1 +
                        "\n\nLatLng2: " + lat2 + ", " + lng2 + "\n\nParkingPricePH: " +
                        mvpModel.getData().getData().getParkingPricePH() + "\n\nClosedTime: " +
                        mvpModel.getData().getData().getCloseTime() + "\n\nOpenTime: " +
                        mvpModel.getData().getData().getOpenTime() + "\n\nstationADAStatus: " +
                        mvpModel.getData().getData().getStationADAStatus() + "\n\nstationId: " +
                        mvpModel.getData().getData().getStationId() + "\n\nZipCode: " +
                        mvpModel.getData().getData().getZipCode() + "\n\nStationStatus: " +
                        mvpModel.getData().getData().getStationStatus() + "\n\nportQuantity: " +
                        mvpModel.getData().getData().getPortQuantity() + "\n\nvendingprice1: " +
                        mvpModel.getData().getData().getVendingprice1() + "\n\nFreeTrail: " +
                        mvpModel.getData().getData().getFreeTrail() + "\n\nvendingPriceUnit1: " +
                        mvpModel.getData().getData().getVendingPriceUnit1() + "\n\nStationAddress: " +
                        mvpModel.getData().getData().getStationAddress() + "\n\nstationName: " +
                        mvpModel.getData().getData().getStationName() + "\n\nnotes: " +
                        mvpModel.getData().getData().getNotes() + "\n\nstationType: " +
                        mvpModel.getData().getData().getStationType() + "\n\nconn1portLevel: " +
                        mvpModel.getData().getData().getConn1portLevel()
                /*+
                mvpModel.getData().getData().getParkingPricePH() + "\n\nLatLng: " + lat1+", "+lng1*/
        );

    }


}
