package com.axxara.axxaraapps.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.axxara.axxaraapps.CaptureActivityPortrait;
import com.axxara.axxaraapps.R;
import com.axxara.axxaraapps.contract.QRContract;
import com.axxara.axxaraapps.presenter.QRPresenter;
import com.axxara.axxaraapps.qrlinkdata.Data;
import com.axxara.axxaraapps.qrlinkdata.QRData;
import com.axxara.axxaraapps.receivers.ScanResultReceiver;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by jaishri on 3/10/18.
 */

public class QRFragment extends Fragment implements ScanResultReceiver{


    @BindView(R.id.txtLink)
    TextView qrLink;
    private View view;
    private String Port1Status;
    private String FreeTrail;
    private String openTime;
    private String StationAddress;
    private String stationName;
    private String notes;
    private String stationType;
    private String conn1portLevel;
    private String parkingPricePH;
    private String qrInfo;
    private String CloseTime, stationADAStatus;
    private String stationId, zip,
            StationStatus;
    private int portQuantity, vendingprice1, vendingPriceUnit1;
    private ArrayList<Double> latLng, latLng2;


    private IntentIntegrator qrScan;
    private Unbinder unbinder;
    private QRPresenter qrPresenter;

    private Intent intent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IntentIntegrator integrator = new IntentIntegrator(getActivity());
        // use forSupportFragment or forFragment method to use fragments instead of activity
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
        //integrator.setResultDisplayDuration(0); // milliseconds to display result on screen after scan
        integrator.setCameraId(0);  // Use a specific camera of the device
        integrator.initiateScan();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Toast.makeText(getActivity(), "Welcom Home", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
        getActivity().startActivityForResult(intent, 0);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.activity_qrcode, container, false);
        unbinder = ButterKnife.bind(this, view);
        /*qrScan = new IntentIntegrator(getActivity());
        qrScan.setOrientationLocked(true);
        qrScan.setBeepEnabled(true);
        qrScan.setCaptureActivity(CaptureActivityPortrait.class);

        qrScan.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
        qrScan.initiateScan();*/
       // qrPresenter = new QRPresenter(this, null);


        return view;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*IntentResult qrResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (qrResult != null) {
            if (qrResult.getContents() == null) {
                Toast.makeText(getActivity(), "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                qrScan = null;
                doStuff(qrResult.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }*/

       /* if (resultCode == RESULT_OK) {
            String contents = intent.getStringExtra("SCAN_RESULT");
            String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
            Toast.makeText(getActivity(),contents,Toast.LENGTH_LONG).show();
            // Handle successful scan
            doStuff(contents);
        } else if (resultCode == RESULT_CANCELED) {
            // Handle cancel
        }*/

        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        /*codeContent = scanningResult.getContents();
        codeFormat = scanningResult.getFormatName();*/
    }

    private void doStuff(final String url) {


        AQuery aQuery = new AQuery(getActivity());
        final ProgressDialog dialog = new ProgressDialog(
                getActivity());
        dialog.setMessage("Please wait ....");

        aQuery.progress(dialog).ajax(url, JSONArray.class,
                new AjaxCallback<JSONArray>() {
                    @Override
                    public void callback(String url,
                                         JSONArray object, AjaxStatus status) {
                        // TODO Auto-generated method stub
                        super.callback(url, object, status);
                        if (status.equals(AjaxStatus.AUTH_ERROR) || status.equals(AjaxStatus.TRANSFORM_ERROR)) {
                            Toast.makeText(getActivity(), "SomeThing went wrong in Authetication ot Transformation.Please check", Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                JSONObject ob = object.optJSONObject(0);

                                JSONArray latLong = ob.optJSONArray("latLng");
                                double Lattitude = latLong.getLong(0);
                                double Longitude = latLong.getLong(1);

                                latLng = new ArrayList<>();
                                latLng.add(Lattitude);
                                latLng.add(Longitude);

                                latLng.addAll(latLng);

                                JSONObject data = ob.getJSONObject("data");

                                JSONArray latLong2 = data.optJSONArray("latLng");
                                double Lattitude2 = latLong2.getLong(0);

                                double Longitude2 = latLong2.getLong(1);
                                Port1Status = data.getString("Port1Status");

                                latLng2 = new ArrayList<>();
                                latLng2.add(Lattitude2);
                                latLng2.add(Longitude2);

                                latLng2.addAll(latLng2);

                                CloseTime = data.getString("CloseTime");
                                stationADAStatus = data.getString("stationADAStatus");
                                stationId = data.getString("stationId");
                                zip = data.getString("ZipCode");
                                StationStatus = data.getString("StationStatus");
                                portQuantity = data.getInt("portQuantity");
                                vendingprice1 = data.getInt("vendingprice1");
                                FreeTrail = data.getString("FreeTrail");
                                vendingPriceUnit1 = data.getInt("vendingPriceUnit1");
                                openTime = data.getString("OpenTime");
                                StationAddress = data.getString("StationAddress");
                                stationName = data.getString("stationName");
                                notes = data.getString("notes");
                                stationType = data.getString("stationType");
                                conn1portLevel = data.getString("conn1portLevel");
                                parkingPricePH = data.getString("parkingPricePH");
                                qrInfo = "\n Port1Status: " + Port1Status + "\nCloseTime: " + CloseTime + "\nstationADAStatus :" + stationADAStatus +
                                        "\nstationId: " + stationId + "\nstationName: " + stationName + "\nzip: " + zip + "\nportQuantity: " + portQuantity
                                        + "\nvendingprice1:" + vendingprice1 + "\nFreeTrail: " + FreeTrail + "\nvendingPriceUnit1: " + vendingPriceUnit1
                                        + "\nopenTime: " + openTime + "\nStationAddress:" + StationAddress + "\nstationName: " + stationName + "\nnotes: " + notes +
                                        "\nstationType: " + stationType + "\nconn1portLevel:" + conn1portLevel + "\nparkingPricePH: " + parkingPricePH;

                                Toast.makeText(getActivity(), "QRInfo"+qrInfo, Toast.LENGTH_SHORT).show();
                                Data qrData = new Data();
                                qrData.setLatLng(latLng2);
                                qrData.setPort1Status("WebLink: " + url + "\n\nPort1Status: " + Port1Status);
                                qrData.setCloseTime(CloseTime);
                                qrData.setConn1portLevel(conn1portLevel);
                                qrData.setZipCode(zip);
                                qrData.setFreeTrail(FreeTrail);
                                qrData.setVendingprice1(vendingprice1);
                                qrData.setVendingPriceUnit1(vendingPriceUnit1);
                                qrData.setOpenTime(openTime);
                                qrData.setStationAddress(StationAddress);
                                qrData.setStationId(stationId);
                                qrData.setStationName(stationName);
                                qrData.setStationStatus(StationStatus);
                                qrData.setNotes(notes);
                                qrData.setStationType(stationType);
                                qrData.setParkingPricePH(parkingPricePH);
                                qrData.setStationADAStatus(stationADAStatus);
                                qrData.setPortQuantity(portQuantity);

                                QRData qrScannedData = new QRData(latLng, qrData);
                                /*qrPresenter = new QRPresenter(getActivity(), qrScannedData);
                                qrPresenter.voidOnScannedCode();*/

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void scanResultData(String codeFormat, String codeContent) {
        Toast.makeText(getActivity(), "DATA:"+codeContent, Toast.LENGTH_SHORT).show();
    }

    /*@Override
    public void initView() {
        unbinder = ButterKnife.bind(getActivity());
    }

    @Override
    public void setData(String string) {
        qrLink.append(string);
    }

    @Override
    public void voidOnScannedCode() {
        qrPresenter.voidOnScannedCode();
    }*/
}
