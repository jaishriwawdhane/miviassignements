package com.axxara.axxaraapps.view;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.axxara.axxaraapps.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class NavDraweActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG_CHART_FRAGMENT = "TAG_CHART_FRAGMENT";
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.chart_fragment_container)
    FrameLayout frameLayout;
    private Unbinder unbinder;

    private Fragment fragment = null;
    private Class fragmentClass = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        overridePendingTransition(0, 0);
        unbinder = ButterKnife.bind(this);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toolbar.setTitle("Charging Activity");
        navigationView.setNavigationItemSelectedListener(this);

        fragmentClass = HomeFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction().replace(R.id.chart_fragment_container, fragment).commitAllowingStateLoss();


    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_home) {
            fragmentClass = HomeFragment.class;
            drawer.closeDrawers();
        } else if (id == R.id.nav_account) {
            frameLayout.setVisibility(View.GONE);
            drawer.closeDrawers();

        } else if (id == R.id.nav_charging) {
            frameLayout.setVisibility(View.VISIBLE);
            fragmentClass = ChartFragment.class;
            drawer.closeDrawers();

        } else if (id == R.id.scan_activity) {
            if (isOnline()) {
                frameLayout.setVisibility(View.GONE);

                startActivity(new Intent(this, QRActivity.class));
//                frameLayout.setVisibility(View.VISIBLE);
//                fragmentClass = QRFragment.class;
                drawer.closeDrawers();

            } else {
                Toast.makeText(this, "Please check yoour internet connection and try again!!", Toast.LENGTH_SHORT).show();
            }
            drawer.closeDrawers();

        } else if (id == R.id.nav_favorites) {
            frameLayout.setVisibility(View.GONE);
            drawer.closeDrawers();

        } else if (id == R.id.nav_favorites) {
            frameLayout.setVisibility(View.GONE);
            drawer.closeDrawers();

        } else if (id == R.id.contat_us) {
            frameLayout.setVisibility(View.GONE);
            drawer.closeDrawers();

        } else if (id == R.id.about_us) {
            frameLayout.setVisibility(View.GONE);
            drawer.closeDrawers();

        } else if (id == R.id.logout) {
            frameLayout.setVisibility(View.GONE);
            drawer.closeDrawers();

        }


        try {

            fragment = (Fragment) fragmentClass.newInstance();

        } catch (Exception e) {

            e.printStackTrace();

        }

        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction().replace(R.id.chart_fragment_container, fragment).commitAllowingStateLoss();

        // Highlight the selected item has been done by NavigationView

        item.setChecked(true);

        // Set action bar title

        setTitle(item.getTitle());

        // Close the navigation drawer

        drawer.closeDrawers();


        return true;
    }


    private boolean isOnline() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnectedOrConnecting()) {

            return true;

        } else {

            return false;

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
