package com.axxara.axxaraapps.view;

import android.annotation.TargetApi;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.axxara.axxaraapps.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by jaishri on 1/10/18.
 */

public class ChartFragment extends Fragment {


    @BindView(R.id.barChart)
    BarChart barChart;
    @BindView(R.id.btnDays)
    Button days;
    @BindView(R.id.btnWeeks)
    Button weeks;
    @BindView(R.id.btnKwh)
    TextView kwh;
    @BindView(R.id.btnSpent)
    TextView spent;
    private View view;
    private boolean isSpentChart = true, iskWhChart;
    private Unbinder unbinder;
    private ArrayList<String> xBar;
    private boolean isDays;

    public ChartFragment() {

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isDays = false;
        isSpentChart = true;
        weeksData("Spent", getResources().getColor(R.color.colorSpentBar), 12.0f, 30.0f, 50.0f, 20.0f);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.activity_chart, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.btnSpent)
    public void onSpentClick() {
        if (!isDays) {
            barChart.invalidate();
            weeksData("Spent", getResources().getColor(R.color.colorSpentBar), 12.0f, 30.0f, 50.0f, 20.0f);
        } else {
            barChart.invalidate();
            daysData("Spent", getResources().getColor(R.color.colorSpentBar), 3.0f, 6.0f, 5.0f, 2.0f, 0.0f, 0.0f, 0.0f);
        }

    }

    @OnClick(R.id.btnKwh)
    public void onKwhClick() {
        if (!isDays) {
            barChart.invalidate();
            weeksData("kwh", getResources().getColor(R.color.colorKwh), 20.0f, 15.0f, 65.0f, 30.0f);
        } else {
            barChart.invalidate();
            daysData("kwh", getResources().getColor(R.color.colorKwh), 9.0f, 5.0f, 3.0f, 8.0f, 0.6f, 0.4f, 0.0f);

        }
    }

    @TargetApi(Build.VERSION_CODES.N)
    private String yesterday(int day) {
        final Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM");
        cal.add(Calendar.DATE, -day);
        return simpleDateFormat.format(cal.getTime());
    }


    private void daysData(String label, int color, Float... val) {


        ArrayList<BarEntry> spentEntries = new ArrayList<>();
        spentEntries.add(new BarEntry(val[0], 0, yesterday(6)));
        spentEntries.add(new BarEntry(val[1], 1, yesterday(5)));
        spentEntries.add(new BarEntry(val[2], 2, yesterday(4)));
        spentEntries.add(new BarEntry(val[3], 3, yesterday(3)));
        spentEntries.add(new BarEntry(val[4], 4, yesterday(2)));

        spentEntries.add(new BarEntry(val[5], 5, yesterday(1)));

        spentEntries.add(new BarEntry(val[6], 6, yesterday(0)));


        BarDataSet dataSet = new BarDataSet(spentEntries, label);
        xBar = new ArrayList<>();
        xBar.add(String.valueOf(yesterday(6)));
        xBar.add(String.valueOf(yesterday(5)));
        xBar.add(String.valueOf(yesterday(4)));
        xBar.add(String.valueOf(yesterday(3)));
        xBar.add(String.valueOf(yesterday(2)));
        xBar.add(String.valueOf(yesterday(1)));
        xBar.add(String.valueOf(yesterday(0)));


        dataSet.setColor(color);
        BarData barData = new BarData(xBar, dataSet);
        barChart.setData(barData);
        barChart.setDescription("");
        barChart.getAxisLeft().setDrawGridLines(false);
        barChart.getXAxis().setDrawGridLines(false);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
    }

    @OnClick(R.id.btnWeeks)
    public void onWeeksClick() {
        isDays = false;
        barChart.invalidate();
//        weeksData("Spent", getResources().getColor(R.color.colorSpentBar), 12.0f, 30.0f, 50.0f, 20.0f);

        if (isSpentChart) {
            weeksData("Spent", getResources().getColor(R.color.colorSpentBar), 12.0f, 30.0f, 50.0f, 20.0f);
        } else {
            isSpentChart = false;
            weeksData("kwh", getResources().getColor(R.color.colorKwh), 20.0f, 15.0f, 65.0f, 30.0f);
        }
    }

    @OnClick(R.id.btnDays)
    public void onDaysClick() {
        isDays = true;
        barChart.invalidate();
        /*daysData("Spent", getResources().getColor(R.color.colorSpentBar), 3.0f, 6.0f, 5.0f, 2.0f, 0.0f, 0.0f, 0.0f);
*/
        if (isSpentChart) {
            daysData("Spent", getResources().getColor(R.color.colorSpentBar), 3.0f, 6.0f, 5.0f, 2.0f, 0.0f, 0.0f, 0.0f);
        } else {
            isSpentChart = false;
            daysData("kwh", getResources().getColor(R.color.colorKwh), 9.0f, 5.0f, 3.0f, 8.0f, 0.6f, 0.4f, 0.0f);

        }


    }

    private void weeksData(String label, int color, Float... val) {

        ArrayList<BarEntry> spentEntries = new ArrayList<>();
        spentEntries.add(new BarEntry(val[0], 0, "Week1"));
        spentEntries.add(new BarEntry(val[1], 1, "Week2"));
        spentEntries.add(new BarEntry(val[2], 2, "Week3"));
        spentEntries.add(new BarEntry(val[3], 3, "Week4"));

        BarDataSet dataSet = new BarDataSet(spentEntries, label);
        xBar = new ArrayList<>();
        xBar.add("Week1");
        xBar.add("Week2");
        xBar.add("Week3");
        xBar.add("Week4");

        dataSet.setColor(color);
        BarData barData = new BarData(xBar, dataSet);
        barChart.setData(barData);
        barChart.setDescription("");
        barChart.getAxisLeft().setDrawGridLines(false);
        barChart.getXAxis().setDrawGridLines(false);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
    }

}
