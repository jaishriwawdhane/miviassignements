package com.axxara.axxaraapps.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import com.axxara.axxaraapps.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by jaishri on 29/9/18.
 */

public class ChartActivity extends AppCompatActivity {

    @BindView(R.id.barChart)
    BarChart barChart;

    @BindView(R.id.btnDays)
    Button days;

    @BindView(R.id.btnWeeks)
    Button weeks;

    @BindView(R.id.btnKwh)
    TextView kwh;

    @BindView(R.id.btnSpent)
    TextView spent;


    private boolean isSpentChart = true, iskWhChart;
    private Unbinder unbinder;
    private ArrayList<String> xBar;
    private boolean isDays;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);
        unbinder = ButterKnife.bind(this);

        isDays = false;
        isSpentChart = true;
        weeksData("Spent", getResources().getColor(R.color.colorSpentBar), 12.0f, 30.0f, 50.0f, 20.0f);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.btnSpent)
    public void onSpentClick() {
        if (!isDays) {
            barChart.invalidate();
            weeksData("Spent", getResources().getColor(R.color.colorSpentBar), 12.0f, 30.0f, 50.0f, 20.0f);
        } else {
            barChart.invalidate();
            daysData("Spent", getResources().getColor(R.color.colorSpentBar), 3.0f, 6.0f, 5.0f, 2.0f, 0.0f, 0.0f, 0.0f);
        }

    }

    @OnClick(R.id.btnKwh)
    public void onKwhClick() {
        if (!isDays) {
            barChart.invalidate();
            weeksData("kwh", getResources().getColor(R.color.colorKwh), 20.0f, 15.0f, 65.0f, 30.0f);
        } else {
            barChart.invalidate();
            daysData("kwh", getResources().getColor(R.color.colorKwh), 9.0f, 5.0f, 3.0f, 8.0f, 0.6f, 0.4f, 0.0f);

        }
    }


    private void daysData(String label, int color, Float... val) {
        ArrayList<BarEntry> spentEntries = new ArrayList<>();
        spentEntries.add(new BarEntry(val[0], 0, "Day1"));
        spentEntries.add(new BarEntry(val[1], 1, "Day2"));
        spentEntries.add(new BarEntry(val[2], 2, "Day3"));
        spentEntries.add(new BarEntry(val[3], 3, "Day4"));
        spentEntries.add(new BarEntry(val[4], 4, "Day5"));

        spentEntries.add(new BarEntry(val[5], 5, "Day6"));

        spentEntries.add(new BarEntry(val[6], 6, "Day7"));


        BarDataSet dataSet = new BarDataSet(spentEntries, label);
        xBar = new ArrayList<>();
        xBar.add("day1");
        xBar.add("day2");
        xBar.add("day3");
        xBar.add("day4");
        xBar.add("day5");
        xBar.add("day6");
        xBar.add("day7");


        dataSet.setColor(color);
        BarData barData = new BarData(xBar, dataSet);
        barChart.setData(barData);
        barChart.setDescription("");

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
    }

    @OnClick(R.id.btnWeeks)
    public void onWeeksClick() {
        isDays = false;
        barChart.invalidate();
//        weeksData("Spent", getResources().getColor(R.color.colorSpentBar), 12.0f, 30.0f, 50.0f, 20.0f);

        if (isSpentChart) {
            weeksData("Spent", getResources().getColor(R.color.colorSpentBar), 12.0f, 30.0f, 50.0f, 20.0f);
        } else {
            isSpentChart = false;
            weeksData("kwh", getResources().getColor(R.color.colorKwh), 20.0f, 15.0f, 65.0f, 30.0f);
        }
    }

    @OnClick(R.id.btnDays)
    public void onDaysClick() {
        isDays = true;
        barChart.invalidate();
        /*daysData("Spent", getResources().getColor(R.color.colorSpentBar), 3.0f, 6.0f, 5.0f, 2.0f, 0.0f, 0.0f, 0.0f);
*/
        if (isSpentChart) {
            daysData("Spent", getResources().getColor(R.color.colorSpentBar), 3.0f, 6.0f, 5.0f, 2.0f, 0.0f, 0.0f, 0.0f);
        } else {
            isSpentChart = false;
            daysData("kwh", getResources().getColor(R.color.colorKwh), 9.0f, 5.0f, 3.0f, 8.0f, 0.6f, 0.4f, 0.0f);

        }


    }

    private void weeksData(String label, int color, Float... val) {

        ArrayList<BarEntry> spentEntries = new ArrayList<>();
        spentEntries.add(new BarEntry(val[0], 0, "Week1"));
        spentEntries.add(new BarEntry(val[1], 1, "Week2"));
        spentEntries.add(new BarEntry(val[2], 2, "Week3"));
        spentEntries.add(new BarEntry(val[3], 3, "Week4"));

        BarDataSet dataSet = new BarDataSet(spentEntries, label);
        xBar = new ArrayList<>();
        xBar.add("Week1");
        xBar.add("Week2");
        xBar.add("Week3");
        xBar.add("Week4");


        dataSet.setColor(color);
        BarData barData = new BarData(xBar, dataSet);
        barChart.setData(barData);
        barChart.setDescription("");

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
    }

}
