package com.axxara.axxaraapps;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.Toast;

import com.axxara.axxaraapps.contract.HomeContract;
import com.axxara.axxaraapps.view.ChartActivity;
import com.axxara.axxaraapps.view.QRActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jaishri on 29/9/18.
 */

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.btnScanner)
    Button scanner;
    @BindView(R.id.btnChart)
    Button chart;


    private HomeContract.HomePresenter homePresenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        overridePendingTransition(0, 0);
        ButterKnife.bind(this);

    }


    @OnClick(R.id.btnChart)
    public void chartOpen() {
        //startActivity(new Intent(this, Scann));
        startActivity(new Intent(this, ChartActivity.class));

    }

    @OnClick(R.id.btnScanner)
    public void sannerOpen() {
        if(isOnline()){
            startActivity(new Intent(this, QRActivity.class));
        }else{
            Toast.makeText(this, "Please check yoour internet connection and try again!!", Toast.LENGTH_SHORT).show();
        }


    }


    private boolean isOnline() {

        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnectedOrConnecting()) {

            return true;

        } else {

            return false;

        }

    }


}
