package com.axxara.axxaraapps.model;

import com.axxara.axxaraapps.contract.QRContract;
import com.axxara.axxaraapps.qrlinkdata.QRData;

/**
 * Created by jaishri on 29/9/18.
 */

public class QRModel implements QRContract.MVPModel {

    private QRData qrData;
    public QRModel(QRData qrData){
        this.qrData = qrData;
    }

    @Override
    public QRData getData() {
        return qrData;
    }
}
