package com.axxara.axxaraapps.model;

import android.content.Intent;

import com.axxara.axxaraapps.HomeActivity;
import com.axxara.axxaraapps.contract.HomeContract;

/**
 * Created by jaishri on 29/9/18.
 */

public class HomeModels implements HomeContract.HomeModel {



    @Override
    public Intent getIntentScanner() {
        return new Intent();
    }

    @Override
    public Intent getQRIntent() {
        return null;
    }
}
