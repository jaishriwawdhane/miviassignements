package com.axxara.axxaraapps.contract;

import android.content.Intent;

/**
 * Created by jaishri on 29/9/18.
 */

public interface HomeContract {

    interface HomeView{
        void initView();
    }

    interface HomeModel{
        Intent getIntentScanner();
        Intent getQRIntent();

    }

    interface HomePresenter{
        void onButtonClicked(android.view.View view);
    }
}
