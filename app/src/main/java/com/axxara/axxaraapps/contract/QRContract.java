package com.axxara.axxaraapps.contract;

import com.axxara.axxaraapps.qrlinkdata.QRData;

/**
 * Created by jaishri on 29/9/18.
 */

public interface QRContract {

    interface MVPView {
        void initView();
        void setData(String string);
    }

    interface MVPModel{
        QRData getData();
    }

    interface MVPPresenter{
       void voidOnScannedCode();
    }
}
